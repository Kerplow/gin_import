module GinImport
  class ImportStrategiesController < ApplicationController
    layout 'application'
    before_action :set_model_list

    def show

    end

    def index
      @import_strategies = GinImport.user_model_name ? ImportStrategy.where(owner_id: method(Ginimport.current_owner).id) : ImportStrategy.all
    end

    def new
      @import_strategy = ImportStrategy.new
    end

    def create
      @import_strategy = ImportStrategy.new(import_strategy_params)
      respond_to do |format|
        if @import_strategy.save
          format.html { redirect_back }
          format.js
        else
          format.html { render :show }
          format.js
        end
      end
    end

    def edit
      @import_strategy = ImportStrategy.find(params[:id])
    end

    def update
      @import_strategy = ImportStrategy.find(params[:id])
      respond_to do |format|
        if @import_strategy.save
          format.html { redirect_back }
          format.js
        else
          format.html { render :show }
          format.js
        end
      end
    end

    def preview_collection
      @url_collection = ApiCollectionService.new(url: params[:url], keys: params[:keys]).call!
      @import_strategy = ImportStrategy.new(session[:import_strategy])
      respond_to do |format|
        format.html { render :new }
        format.js
      end
    end

    def preview_source
      if params[:id].present?
        @import_strategy = ImportStrategy.find(params[:id])
        if params[:import_strategy].present?
          @import_strategy.assign_attributes(import_strategy_params)
        end
      else
        @import_strategy = ImportStrategy.new(import_strategy_params)
      end
      session[:import_strategy] = import_strategy_params
      generate_preview
    end

    def test_cypher
      @import_strategy = params[:id] ? ImportStrategy.find(params[:id]) : @import_strategy = ImportStrategy.new(session[:import_strategy])
      @preview = JSON.parse(params[:preview])
      @cypher = cypher_params
      session[:cypher] = cypher_params
      @import_strategy.cypher = @cypher
      @test = StrategyTestService.new(preview: @preview, cypher: @cypher.to_hash, import_strategy: @import_strategy).call!
      respond_to do |format|
        format.html { render :new }
        format.js
      end
    end

    def commit_import
      @import_strategy = params[:id] ? ImportStrategy.find(params[:id]) : @import_strategy = ImportStrategy.new(session[:import_strategy])
      @cypher = session[:cypher]
      @import_strategy.cypher = @cypher
      service = ImportService.new(import_strategy: @import_strategy)
      @result = service.call!
      respond_to do |format|
        format.html { render :new }
        format.js
      end
    end

    private

    def set_model_list
      @models = GinImport.get_model_list
    end

    def import_strategy_params
      params.require(:import_strategy).permit(:model, :multiple, :url, :url_type, :url_collection, :document_type)
    end

    def cypher_params
      params.require(:model).permit!
    end

    def generate_preview
      if @import_strategy.valid?
        @preview = PreviewService.new(import_strategy: @import_strategy).call!
      end
      respond_to do |format|
        format.html { render :new }
        format.js
      end
    end
  end
end
