require 'open-uri'

module GinImport
  class ApiCollectionService < Base
    def initialize(args)
      @url  = args[:url]
      @keys = args[:keys]
    end

    private

    def service_call
      data = JSON.parse(open(@url).read)
      get_ids(data)
    end

    def get_ids(data)
      /^(?<pre>.*?),?(\[\]),?(?<after>.*?)$/ =~ @keys
      if pre
        data = data.dig(*pre.split(','))
      end
      data.map do |d|
        if after
          d.dig(*after.split(','))
        end
      end
    end
  end
end
