require 'open-uri'

module GinImport
  class ImportService < Base

    private

    def initialize(args = nil)
      super
      if @import_strategy.dynamic?
        if @import_strategy.url_collection =~ /^(\w+)-(\w+)$/
          @collection = *("#{$1}".."#{$2}")
        else
          @collection = @import_strategy.url_collection.split(/ ?, ?/)
        end
      end
    end

    def service_call
      raise ArgumentError, 'No cypher found' unless @cypher

      case @import_strategy.document_type
      when 'json'
        service = Import::JsonImportService
      else
        raise ArgumentError, 'unsupported document_type'
      end

      if @import_strategy.dynamic?
        @result = []
        @collection.each do |index|
          url = @url = @import_strategy.url.gsub(':id', index)

          if data = get_data(url)
            @result << service.new(import_strategy: @import_strategy, cypher: @cypher, data: data).call!
          else
            @result << errors[url]
          end
        end
      else
        @result = service.new(import_strategy: @import_strategy, cypher: @cypher, data: get_data(url)).call!
      end
      return @result
    end
  end
end
