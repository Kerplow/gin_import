module GinImport
  class StrategyTestService < Base
    def initialize(**kwargs)
      super
      @data = kwargs[:preview]
    end

    private

    def service_call
      raise ArgumentError.new 'no cypher provided' unless @cypher
      return StrategyTest::JsonTestService.new(import_strategy: @import_strategy, cypher: @cypher, data: @data).send(:service_call) if @import_strategy.json?
    end
  end
end
