module GinImport
  class PreviewService < ImportService

    private

    def service_call
      return Preview::JsonPreviewService.new(import_strategy: @import_strategy).call! if @import_strategy.json?
    end
  end
end
