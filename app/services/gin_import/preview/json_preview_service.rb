require 'open-uri'

module GinImport
  module Preview
    class JsonPreviewService < PreviewService
      private

      def service_call
        preview
      end

      def preview
        if @import_strategy.dynamic?
          @url = @import_strategy.url.gsub(':id', @collection.first)
        else
          @url = @import_strategy.url
        end

        begin
          contents = open(@url).read
        rescue OpenURI::HTTPError => e
          @error = e
          false
        else
          JSON.parse(contents)
        end
      end
    end
  end
end
