module GinImport
  class Base
    attr_reader :import_strategy, :cypher, :data, :error

    include ActiveModel::Validations

    def initialize(args)
      @import_strategy = args[:import_strategy]
      @cypher = args[:cypher] || @import_strategy.cypher
      @data = args[:data]
    end

    def call!
      service_call
    end

    def call
      begin
        result = service_call
      rescue StandardError => e
        @error = e
        false
      else
        result
      end
    end

    def get_data(url)
      begin
        data = URI.open(url).read
      rescue OpenURI::HTTPError => e
        errors.add url, e
        false
      else
        data
      end
    end
  end
end
