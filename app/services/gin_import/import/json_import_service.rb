module GinImport
  module Import
    class JsonImportService < ImportService
      def initialize(args = nil)
        super
        @data = JSON.parse(@data)
      end

      private

      def service_call
        if @import_strategy.multiple?
          @data = collection.first
        end
        new_model = parse_model
        new_model.save ? new_model : false
      end

      def collection
        @data.dig(*@cypher.delete('collection').split(/ *, */).map { |i| Integer(i) rescue i })
      end

      def parse_model
        new_model = @import_strategy.model.constantize.new
        @cypher.each do |key, value|
          from, to = value['regex'].values
          new_model[key.to_sym] = @data.dig(*value['shovel'].split(/ *, */).map { |i| Integer(i) rescue i }) # .gsub(from, to)
        end
        new_model
      end
    end
  end
end
