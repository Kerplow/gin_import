module GinImport
  class ImportStrategy < ApplicationRecord
    self.table_name = GinImport.config.import_table_name

    URLCOLLECTIONTYPES = [:range, :csv, :api]

    validates_presence_of :model ,:url ,:document_type

    belongs_to :owner, class_name: GinImport.config.user_model_name.to_s.camelize if GinImport.config.user_model_name


    enum document_type: { json: 0 }
    enum url_type: { static: 0, dynamic: 1 }
  end
end
