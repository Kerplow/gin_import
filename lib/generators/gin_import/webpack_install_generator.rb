module GinImport
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path('templates', __dir__)

      def base_configuration
        @import_table_name = 'import_strategies'
        if @user_model_name = yes?('assign to owner?(y/n)')
          @user_model_name = ask('what is the name of the owner class?')
        end
      end
    end
  end
end
