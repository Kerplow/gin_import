module GinImport
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path('templates', __dir__)

      def base_configuration
        @import_table_name = 'import_strategies'
        if @user_model_name = yes?('assign to owner?(y/n)')
          @user_model_name = ask('what is the name of the owner class?')
        end
      end

      def create_migration_file
        timestamp = Time.now.strftime("%Y%m%d%H%M%S")
        template "migration_template.erb", "db/migrate/#{timestamp}_create_#{@import_table_name}.rb"
      end

      def create_initializer_file
        template 'initializer_template.erb', "config/initializers/gin_import.rb"
      end

      def configuration_comments
        readme "README"
      end
    end
  end
end
