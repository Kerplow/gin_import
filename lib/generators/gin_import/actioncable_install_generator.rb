module GinImport
  module Generators
    class ActioncableInstallGenerator < Rails::Generators::Base
      source_root File.expand_path('templates', __dir__)

      def base_configuration
        @already_set_up = yes?('is there an existing actioncable installation? (y/n)')
        @channel_name = ask('channel name to use?')
      end

      def create_actioncable_setup
        # base setup here
      end

      def create_channel

      end

      def create_subscription

      end

      def configuration_comments
        readme "actioncable_readme"
      end
    end
  end
end
