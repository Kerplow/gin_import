require "gin_import/engine"

module GinImport
  # in case the db table name is already in use
  mattr_accessor :import_table_name
  @@import_table_name = :import_strategies

  # owner model in the belongs_to :owner for strategies. nil means strategies have no associated owner model
  mattr_accessor :user_model_name
  @@user_model_name = nil

  # method to use to grab the relevant owner record when scoping strategies
  mattr_accessor :current_owner
  @@current_owner = nil

  # include application_record as a default
  mattr_accessor :allowed_models
  @@allowed_models = ['ApplicationRecord']

  # false by default to lure people to the initializer
  mattr_accessor :include_descendants
  @@include_descendants = false

  # application_record can't be parsed
  mattr_accessor :restricted_models
  @@restricted_models = ['ApplicationRecord']

  # model list will be parsed from allowed and restricted models
  mattr_accessor :model_list
  @@model_list = nil

  # async adapter (none, activejob or sidekiq)
  mattr_accessor :async_adapter
  @@async_adapter = nil

  # actioncable channel (nil means no action cable, empty string is no channel)
  mattr_accessor :actioncable_channel
  @@actioncable_channel = nil

  def self.setup
    yield(self)
  end

  def self.config
    self
  end

  def self.get_model_list
    # return @@model_list if @@model_list
    Rails.application.eager_load!
    @@model_list = config.include_descendants ? config.allowed_models.map { |m| [m.constantize, *m.constantize.descendants] }.flatten : config.allowed_models.map(&:constantize)
    @@model_list -= config.restricted_models.map(&:constantize)
    return @@model_list.empty? ? [nil, '# empty by default, see initializers/gin_import to change which models are allowed'] : @@model_list
  end
end
