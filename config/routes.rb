GinImport::Engine.routes.draw do
  root to: 'import_strategies#new'
  resources :import_strategies, only: [:index, :show, :new, :create]
  get  '/import'            , to: 'import_strategies#new'
  post '/preview_source'    , to: 'import_strategies#preview_source'    , as: :preview
  post '/test_cypher'       , to: 'import_strategies#test_cypher'       , as: :test
  post '/import'            , to: 'import_strategies#commit_import'     , as: :commit_import
  get  '/preview_collection', to: 'import_strategies#preview_collection', as: :preview_collection
end
