$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "gin_import/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "gin_import"
  spec.version     = GinImport::VERSION
  spec.authors     = ["Charles de Gheldere"]
  spec.email       = ["charlesdegh@gmail.com"]
  spec.homepage    = "https://github.com/kerplow/gin_import"
  spec.summary     = "easily fill your database with record from the internet"
  spec.description = "Gin_import is an (en)gin(e) that allows quick imports of records online, be it json or html. Import strategies can be executed on demand or saved and executed asynchronously"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 7.0"
  spec.add_dependency "simple_form", "~> 5.0.3", ">= 5.0.3"
  spec.add_dependency 'bootstrap', '~> 4.3.1', ">= 4.3.1"

  spec.add_development_dependency "sqlite3"
end
